# Dockerfile for http://www.powershellempire.com/
FROM fedora

LABEL maintainer="mattias.ohlsson@inprose.com"

# ImportError: No module named flask
# dnf -y install python-flask
# ImportError: No module named OpenSSL
# dnf -y install python2-pyOpenSSL
# ImportError: No module named Crypto.Random
# dnf -y install python2-crypto
# ImportError: No module named pydispatch
# dnf -y install python2-pydispatcher
# ImportError: No module named iptools
# dnf -y install python-iptools
# ImportError: No module named netifaces
# dnf -y install python2-netifaces
# ImportError: No module named zlib_wrapper
# pip install zlib_wrapper
# ImportError: No module named M2Crypto
# dnf -y install m2crypto
# ImportError: No module named macholib.MachO
# pip install macholib
# ImportError: No module named dropbox
# dnf -y install python-dropbox
# ImportError: No module named pyminifier
# pip install pyminifier

RUN dnf -y update && dnf -y install git python-flask python2-pyOpenSSL python2-crypto python2-pydispatcher python-iptools python2-netifaces m2crypto python-dropbox which && dnf clean all
RUN pip install zlib_wrapper macholib pyminifier

# https://github.com/PowerShell/PowerShell/blob/master/docs/installation/linux.md
RUN rpm --import https://packages.microsoft.com/keys/microsoft.asc
RUN curl https://packages.microsoft.com/config/rhel/7/prod.repo > /etc/yum.repos.d/microsoft.repo
RUN dnf -y install compat-openssl10 powershell && dnf clean all

RUN ln -s /usr/bin/pwsh /usr/bin/powershell

RUN git clone https://github.com/PowerShellEmpire/Empire.git /opt/empire

# setup/setup_database.py:
# Staging Key is set up via environmental variable
# or via command line. By setting RANDOM a randomly
# selected password will automatically be selected
# or it can be set to any bash acceptable character
# set for a password.
ENV STAGING_KEY=RANDOM
RUN cd /opt/empire/setup; ./setup_database.py

WORKDIR /opt/empire
CMD if [ -t 0 ]; then ./empire; fi
